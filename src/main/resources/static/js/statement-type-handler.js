let modal = document.getElementById('myModal');

let btn = document.getElementById("myBtn");

let span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
if (btn != null) {
    btn.onclick = function () {
        modal.style.display = "block";
    };

}

if (modal != null) {

    $('#selectStatementType').on('change', function () {
        window.location.replace("/" + this.value.toLowerCase() + "-statement");

    });
}

if (span != null) {
// When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    };
}

// When the user clicks anywhere outside of the modal, close it
if (modal != null) {
    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    };
}

let key = "2dfa67e4d09c4dc180e171318180502";
let baseUrl = "http://api.apixu.com/v1/current.json?key=" + key + "&q=Yerevan";
//Make synchronous request to weather api
let req = new XMLHttpRequest();
req.open('GET', baseUrl, false);
req.send(null);
if (req.status === 200) {
    let obj = JSON.parse(req.responseText);
    let val = obj['current'];
    $("#weather").text(val['temp_c']);
}