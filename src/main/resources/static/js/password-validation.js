function validatePassword(password) {
    let regExp = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*)[\w!@#$%^&*]{6,}$/;
    return regExp.test(password);
}


function validateP() {
    let result = $('#resultPassword');
    let password = $('input[name=password]').val();
    if (validatePassword(password)) {
        result.text("Password is valid :)");
        result.css('color', 'green')
    }

    else {
        result.text("Password is not valid :(");
        result.css('color', 'red')
    }
}

let typingTimerP;
const doneTypingIntervalP = 1500;

$('#password').keyup(function () {
    clearTimeout(typingTimerP);
    if ($('input[name=password]').val()) {
        typingTimerP = setTimeout(validateP, doneTypingIntervalP);
    }
});