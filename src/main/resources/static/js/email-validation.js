/**              Email address validation          **/

function validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function validateE() {
    let result = $("#resultEmail");
    let email = $("input[name=email]").val();
    result.text("");
    if (validateEmail(email)) {
        let exists = sendRequest();
        if (exists === false) {
            result.text("Email is valid :)");
            result.css("color", "green");
        }
    } else {
        result.text("Email is not valid :(");
        result.css('color', 'red');
    }
}

let typingTimer;
const doneTypingInterval = 2000;

$('input[name=email]').keyup(function () {
    clearTimeout(typingTimer);
    if ($('input[name=email]').val()) {
        typingTimer = setTimeout(validateE, doneTypingInterval);
    }
});


/****  Check if user email already exists   ***/

function sendRequest() {

    let exists = false;

    fetch('http://localhost:8080/user/' + $('input[name=email]').val(), {
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'text/plain'
            })
        },
    ).then(response => {
        if (response.ok) {
            response.text().then(text => {
                if (text === 'true') {
                    exists = true;
                    let result = $("#resultEmail");
                    result.text("This Email has already been used");
                    result.css('color', 'red');
                    return;
                }

                exists = false;

            });
        }
    });

    return exists;
}