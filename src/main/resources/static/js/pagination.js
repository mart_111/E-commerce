document.ready = (function () {
    changePageAndSize();
});

function changePageAndSize() {
    $('#pageSizeSelect').change(function () {
        let url = window.location.href;
        let index = url.lastIndexOf('s');
        let search = url.substring(index + 2, url.length);
        window.location.replace("/get?pageSize=" + this.value + "&page=1" + "&s=" + search);
        console.log(this.value);
    });
}
