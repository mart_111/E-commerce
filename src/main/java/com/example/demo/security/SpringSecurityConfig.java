package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {


    private final AccessDeniedHandler accessDeniedHandler;

    private final UserDetailsService userDetailsService;

    @Autowired
    public SpringSecurityConfig(AccessDeniedHandler accessDeniedHandler, @Qualifier("customUserDetailsService") UserDetailsService userDetailsService) {
        this.accessDeniedHandler = accessDeniedHandler;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors()
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/app-login").permitAll()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/user/{email}").permitAll()
                .antMatchers("/sign-up").permitAll()
                .antMatchers("/registration-confirm").permitAll()
                .antMatchers("/home", "home/**").permitAll()
                .antMatchers("/get", "/get/**").permitAll()
                .antMatchers("/upload", "/upload/**").permitAll()
                .antMatchers("/job-statement", "/job-statement/**").permitAll()
                .antMatchers("/vehicle-statement", "/vehicle-statement/**").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/app-login")
                .usernameParameter("email").passwordParameter("pass")
                .loginProcessingUrl("/perform-login")
                .successHandler((request, response, authentication) -> response.sendRedirect("/home"))
                .and()
                .logout()
                .logoutUrl("/perform-logout")
                .logoutSuccessHandler((request, response, authentication) -> response.sendRedirect("/app-login"))
                .clearAuthentication(true).invalidateHttpSession(true).deleteCookies("JSESSIONID", "remember-me")
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler).and()
                .httpBasic().disable();
    }

    @Bean
    public PasswordEncoder bCryptPasswordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
