package com.example.demo.model;


import com.example.demo.model.document.JobClassifieds;
import com.example.demo.model.document.VehicleClassifieds;

/**
 * Base,marker interface for all type of Classifieds {@link JobClassifieds},
 * {@link VehicleClassifieds} etc.
 */

public interface Classifieds {
}
