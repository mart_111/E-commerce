package com.example.demo.model.document;

import com.example.demo.model.Classifieds;
import com.example.demo.model.Image;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Document
public class VehicleClassifieds implements Classifieds {

    @Id
    private String id;
    private String title;
    private String description;
    private int price;
    private String make;
    private String model;
    private int year;
    private String location;
    private int mileage;
    private String steeringWheel;
    private Date createdAt;

    private Set<Image> images;

    public VehicleClassifieds() {
        createdAt = new Date();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getSteeringWheel() {
        return steeringWheel;
    }

    public void setSteeringWheel(String steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleClassifieds that = (VehicleClassifieds) o;
        return price == that.price &&
                year == that.year &&
                mileage == that.mileage &&
                Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(images, that.images) &&
                Objects.equals(make, that.make) &&
                Objects.equals(model, that.model) &&
                Objects.equals(location, that.location) &&
                Objects.equals(steeringWheel, that.steeringWheel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, images, price, make, model, year, location, mileage, steeringWheel);
    }

    @Override
    public String toString() {
        return "VehicleClassifieds{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", images=" + images +
                ", price=" + price +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", location='" + location + '\'' +
                ", mileage=" + mileage +
                ", steeringWheel='" + steeringWheel + '\'' +
                '}';
    }
}
