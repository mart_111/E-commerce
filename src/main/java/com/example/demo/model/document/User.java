package com.example.demo.model.document;

import com.example.demo.model.Role;
import com.example.demo.validation.ValidPassword;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Document
public class User {
    @Id
    private String id;
    @Email
    private String email;
    @Size(min = 1, max = 60, message = "Name must be a bit longer")
    private String name;
    @ValidPassword
    @Size(min = 6, message = "Password must contain at least 6 characters")
    private String password;
    @Size(min = 1, max = 60, message = "Last name must be a bit longer")
    private String lastName;
    private int active;
    private String gender;
    private Date registeredDate;
    private int yearOfBirth;
    private int dayOfBirth;
    private String monthOfBirth;
    private Set<Role> roles;

    private Set<VehicleClassifieds> vehicleClassifiedsList;

    private Set<JobClassifieds> jobClassifiedsList;

    public User() {
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public int getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(int dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public String getMonthOfBirth() {
        return monthOfBirth;
    }

    public void setMonthOfBirth(String monthOfBirth) {
        this.monthOfBirth = monthOfBirth;
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date date) {
        this.registeredDate = date;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<VehicleClassifieds> getVehicleClassifiedsList() {
        return vehicleClassifiedsList;
    }

    public void setVehicleClassifiedsList(Set<VehicleClassifieds> vehicleClassifiedsList) {
        this.vehicleClassifiedsList = vehicleClassifiedsList;
    }

    public Set<JobClassifieds> getJobClassifiedsList() {
        return jobClassifiedsList;
    }

    public void setJobClassifiedsList(Set<JobClassifieds> jobClassifiedsList) {
        this.jobClassifiedsList = jobClassifiedsList;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", lastName='" + lastName + '\'' +
                ", active=" + active +
                ", gender='" + gender + '\'' +
                ", registeredDate=" + registeredDate +
                ", yearOfBirth=" + yearOfBirth +
                ", dayOfBirth=" + dayOfBirth +
                ", monthOfBirth='" + monthOfBirth + '\'' +
                ", roles=" + roles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) &&
                active == user.active &&
                yearOfBirth == user.yearOfBirth &&
                dayOfBirth == user.dayOfBirth &&
                Objects.equals(email, user.email) &&
                Objects.equals(name, user.name) &&
                Objects.equals(password, user.password) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(gender, user.gender) &&
                Objects.equals(registeredDate, user.registeredDate) &&
                Objects.equals(monthOfBirth, user.monthOfBirth) &&
                Objects.equals(roles, user.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, name, password, lastName, active, gender, registeredDate, yearOfBirth, dayOfBirth, monthOfBirth, roles);
    }
}
