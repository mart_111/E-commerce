package com.example.demo.model;

import java.util.Objects;

public class Image {

    private String id;
    private String imageRef;

    public String getImageRef() {
        return imageRef;
    }

    public void setImageRef(String imageRef) {
        this.imageRef = imageRef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return Objects.equals(id, image.id) &&
                Objects.equals(imageRef, image.imageRef);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, imageRef);
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", imageRef='" + imageRef + '\'' +
                '}';
    }
}
