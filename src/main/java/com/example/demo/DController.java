package com.example.demo;

import com.example.demo.model.Pager;
import com.example.demo.model.document.JobClassifieds;
import com.example.demo.repository.UserRepo;
import com.example.demo.service.JobClassifiedsService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Principal;
import java.util.Optional;

@Controller
public class DController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 30;
    private static final int[] PAGE_SIZES = {INITIAL_PAGE_SIZE, 20, 30, 50};

    private final UserService userService;
    @Autowired
    JobClassifiedsService service;
    @Autowired
    UserRepo repo;

    @Autowired
    public DController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/get")
    public ModelAndView getAllMatching(@RequestParam("pageSize") Optional<Integer> pageSize,
                                       @RequestParam("page") Optional<Integer> page,
                                       @RequestParam("s") Optional<String> s) {


        ModelAndView mav = new ModelAndView("persons");
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        Page<JobClassifieds> classifieds = service.getAllByTitle(s.orElse(""), PageRequest.of(evalPage, evalPageSize));
        Pager pager = new Pager(classifieds.getTotalPages(), classifieds.getNumber(), BUTTONS_TO_SHOW);

        mav.addObject("persons", classifieds);
        mav.addObject("selectedPageSize", evalPageSize);
        mav.addObject("pageSizes", PAGE_SIZES);
        mav.addObject("pager", pager);
        mav.addObject("search", s.orElse(""));
        return mav;
    }

    @GetMapping("/upload")
    public String uploadGet() {
        return "upload";
    }


    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file, HttpServletRequest req) throws IOException {
        if (!file.isEmpty()) {

            String fileName = file.getOriginalFilename();
            InputStream is = file.getInputStream();

            String path = "http://localhost:8080" + "/src/main/resources/uploaded/";
            Files.copy(is, Paths.get(path + fileName),
                    StandardCopyOption.REPLACE_EXISTING);

            return "uploaded";

        } else {

            System.out.println("error while uploading");
        }

        return "error";
    }

    @GetMapping("/vehicle-statement")
    public String getVehiclePage() {
        return "vehicle-statement";
    }

    @PostMapping("/vehicle-statement")
    @ResponseBody
    public String addVehicleStatement(Principal principal) {

//        VehicleClassifieds statement1 = new VehicleClassifieds();
//        statement1.setTitle("taza ban 3");
//
//        User user = userService.getUserByEmail(principal.getName());
//        user.getVehicleClassifiedsList().addAll(Set.of(statement1));
//        userService.save(user);
        return "added";
    }

    @GetMapping("/job-statement")
    public String getJobStatementPage() {
        return "job-statement";
    }
}

