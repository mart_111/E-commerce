package com.example.demo.controllers;

import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DemoController {

    private final UserService userService;

    @Autowired
    public DemoController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/")
    public ModelAndView welcomePage() {
        return new ModelAndView("welcome-page");
    }

    @GetMapping("/user/{email}")
    @ResponseBody
    public boolean getUserByEmail(@PathVariable String email) {
        return userService.isEmailAlreadyInUse(email);
    }

    @GetMapping("/app-login")
    public ModelAndView reachLoginPage(HttpServletRequest req) {
        return new ModelAndView("custom-login");
    }

    @GetMapping("/home")
    public ModelAndView home() {
        return new ModelAndView("home-page");
    }

    @GetMapping("/error")
    public ModelAndView errorMessage() {
        return new ModelAndView("error");
    }

    @GetMapping("/403")
    public String page403() {
        return "403";
    }
}
