package com.example.demo.controllers;

import com.example.demo.model.Role;
import com.example.demo.model.document.User;
import com.example.demo.model.document.VerificationToken;
import com.example.demo.repository.VerificationTokenRepo;
import com.example.demo.service.UserService;
import com.example.demo.service.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Date;

@Controller
public class RegistrationController {

    private final UserService userService;
    private final VerificationTokenService tokenService;
    private final VerificationTokenRepo tokenRepo;

    @Autowired
    public RegistrationController(UserService userService, VerificationTokenService tokenService, VerificationTokenRepo tokenRepo) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.tokenRepo = tokenRepo;
    }


    @GetMapping("/sign-up")
    public String registration(User user) {
        return "register";
    }

    @PostMapping("/sign-up")
    public ModelAndView performRegistration(@ModelAttribute("user") @Valid User user,
                                            BindingResult bindingResult,
                                            @RequestParam String password,
                                            @RequestParam String confirmPassword) {
        ModelAndView modelAndView = new ModelAndView();
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        modelAndView.addObject("pass", password);
        modelAndView.addObject("confPass", confirmPassword);
        if (bindingResult.hasErrors() || !password.equals(confirmPassword)) {
            modelAndView.setViewName("register");
            modelAndView.addObject("confirmPassError", "Please enter correct password");
            return modelAndView;
        }

        user.setPassword(encoder.encode(password));
        user.setRoles(Collections.singleton(new Role("USER")));
        user.setRegisteredDate(new Date());

        try {
            userService.save(user);
            tokenService.sendVerificationToken(user);
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            modelAndView.addObject("uniqueEmailException", "This email has already been used.");
            modelAndView.setViewName("register");
            return modelAndView;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        modelAndView.setViewName("email-verification");
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @GetMapping("/registration-confirm")
    public ModelAndView confirmed(@RequestParam String token) {
        VerificationToken myToken = tokenRepo.findByToken(token);
        ModelAndView modelAndView = new ModelAndView();
        if (myToken == null) {

            System.out.println("token is not valid");
            modelAndView.addObject("notFound", "Verification token isn't found");
            modelAndView.setViewName("error");
            return modelAndView;
        }

        User user = myToken.getUser();

        if (userService.checkIfTokenExpired(myToken.getToken())) {
            System.out.println("Token was expired. try again");
            modelAndView.addObject("expired", "Your token was expired");
            modelAndView.setViewName("error");
            return modelAndView;
        }

        User updatedUser = userService.getUserById(user.getId());
        updatedUser.setActive(1);
        userService.save(updatedUser);
        tokenRepo.delete(myToken);
        modelAndView.setViewName("home-page");
        return modelAndView;
    }

}
