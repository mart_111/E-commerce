package com.example.demo.service;

import com.example.demo.model.document.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.UUID;

@Service
public class VerificationTokenService {

    private JavaMailSender javaMailSender;
    private UserService userService;
    private String token;

    @Autowired
    public VerificationTokenService(JavaMailSender javaMailSender, UserService userService) {
        this.javaMailSender = javaMailSender;
        this.userService = userService;
    }

    @Async("threadPoolTaskExecutor")
    public void sendVerificationToken(User user) throws MailException, MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        token = UUID.randomUUID().toString();
        userService.createVerificationTokenForUser(token, user);
        helper.setTo(user.getEmail());
        helper.setSubject("Hello! Dear " + user.getName() + " this is Confirmation Email.\r\n" + "Please check below");
        helper.setFrom("mart34111@gmail.com");

        helper.setText("<html>" +
                "<body>" +
                "Check link below." +
                "<a href='http://localhost:8080/registration-confirm?token=" + token + "'>Confirm your email" +
                "</a>" +
                "</body>" +
                "</html>", true);

        javaMailSender.send(mimeMessage);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getToken() {
        return token;
    }
}
