package com.example.demo.service;

import com.example.demo.model.document.VehicleClassifieds;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface VehicleClassifiedsService {

    List<VehicleClassifieds> getAll();

    Page<VehicleClassifieds> getAllByTitle(String title, Pageable pageable);

    void save(VehicleClassifieds classifieds);

}
