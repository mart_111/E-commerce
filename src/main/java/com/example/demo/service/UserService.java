package com.example.demo.service;

import com.example.demo.model.document.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    void save(User user);

    User getUserById(String id);

    List<User> getAllUsers();

    User getUserByEmail(String email);

    void createVerificationTokenForUser(String token, User user);

    boolean isEmailAlreadyInUse(String email);

    boolean checkIfTokenExpired(String token);

    Page<User> findUserByName(String name, Pageable pageable);

}
