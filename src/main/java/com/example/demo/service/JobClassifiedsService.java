package com.example.demo.service;

import com.example.demo.model.document.JobClassifieds;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JobClassifiedsService {

    List<JobClassifieds> getAll();

    Page<JobClassifieds> getAllByTitle(String title, Pageable pageable);

    void save(JobClassifieds classifieds);
}
