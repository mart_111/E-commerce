package com.example.demo.service.impl;

import com.example.demo.model.document.JobClassifieds;
import com.example.demo.repository.JobClassifiedsRepo;
import com.example.demo.service.JobClassifiedsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobClassifiedsServiceImpl implements JobClassifiedsService {


    private JobClassifiedsRepo repo;

    @Autowired
    public JobClassifiedsServiceImpl(JobClassifiedsRepo repo) {
        this.repo = repo;
    }

    @Override
    public List<JobClassifieds> getAll() {
        return repo.findAll(Sort.by("createdAt").descending());
    }

    @Override
    public Page<JobClassifieds> getAllByTitle(String title, Pageable pageable) {
        return repo.findAllByTitleOrderByCreatedAtDesc(title, pageable);
    }

    @Override
    public void save(JobClassifieds classifieds) {
        repo.save(classifieds);
    }
}
