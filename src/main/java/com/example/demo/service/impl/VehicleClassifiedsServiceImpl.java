package com.example.demo.service.impl;

import com.example.demo.model.document.VehicleClassifieds;
import com.example.demo.repository.VehicleClassifiedsRepo;
import com.example.demo.service.VehicleClassifiedsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleClassifiedsServiceImpl implements VehicleClassifiedsService {

    private VehicleClassifiedsRepo repo;

    @Autowired
    public VehicleClassifiedsServiceImpl(VehicleClassifiedsRepo repo) {
        this.repo = repo;
    }

    @Override
    public List<VehicleClassifieds> getAll() {
        return repo.findAll(Sort.by("createdAt").descending());
    }

    @Override
    public Page<VehicleClassifieds> getAllByTitle(String title, Pageable pageable) {
        return repo.findAllByTitleOrderByCreatedAtDesc(title, pageable);
    }

    @Override
    public void save(VehicleClassifieds classifieds) {
        repo.save(classifieds);
    }
}
