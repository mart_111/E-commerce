package com.example.demo.service.impl;

import com.example.demo.model.document.User;
import com.example.demo.model.document.VerificationToken;
import com.example.demo.repository.UserRepo;
import com.example.demo.repository.VerificationTokenRepo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;

    private final VerificationTokenRepo tokenRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, VerificationTokenRepo tokenRepo) {
        this.userRepo = userRepo;
        this.tokenRepo = tokenRepo;
    }

    @Override
    public void save(User user) {
        userRepo.save(user);
    }

    @Override
    public User getUserById(String id) {
        return userRepo.findUserById(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepo.findByEmail(email);

    }

    @Override
    public void createVerificationTokenForUser(String token, User user) {
        final VerificationToken myToken = new VerificationToken(token, user);
        tokenRepo.save(myToken);
    }


    @Override
    public boolean isEmailAlreadyInUse(String email) {
        return userRepo.existsByEmail(email);
    }

    @Override
    public boolean checkIfTokenExpired(String token) {
        VerificationToken myToken = tokenRepo.findByToken(token);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());
        Date expirationDate = myToken.getExpirationDate();
        if (expirationDate.getTime() - cal.getTime().getTime() <= 0) {
            tokenRepo.delete(myToken);
            return true;
        }
        return false;
    }

    @Override
    public Page<User> findUserByName(String name, Pageable pageable) {
        return userRepo.findUserByName(name, pageable);
    }
}
