package com.example.demo.repository;

import com.example.demo.model.document.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepo extends MongoRepository<User, Integer> {

    User findByEmail(String email);

    User findUserById(String id);

    Page<User> findUserByName(String name, Pageable pageable);

    Page<User> findAll(Pageable pageable);

    boolean existsByEmail(String email);
}
