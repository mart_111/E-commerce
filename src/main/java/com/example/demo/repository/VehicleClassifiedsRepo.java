package com.example.demo.repository;

import com.example.demo.model.document.VehicleClassifieds;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface VehicleClassifiedsRepo extends MongoRepository<VehicleClassifieds, String> {

    Page<VehicleClassifieds> findAllByTitleOrderByCreatedAtDesc(String title, Pageable pageable);
}
