package com.example.demo.repository;

import com.example.demo.model.document.JobClassifieds;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JobClassifiedsRepo extends MongoRepository<JobClassifieds, String> {

    Page<JobClassifieds> findAllByTitleOrderByCreatedAtDesc(String title, Pageable pageable);
}
