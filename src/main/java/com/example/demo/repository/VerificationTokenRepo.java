package com.example.demo.repository;

import com.example.demo.model.document.User;
import com.example.demo.model.document.VerificationToken;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface VerificationTokenRepo extends MongoRepository<VerificationToken, Integer> {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);
}
